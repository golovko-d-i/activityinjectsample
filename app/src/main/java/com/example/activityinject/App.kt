package com.example.activityinject

import android.app.Application
import com.example.activityinject.di.ActivityModule
import com.example.activityinject.di.ApplicationComponent
import com.example.activityinject.di.ApplicationModule
import com.example.activityinject.di.DaggerApplicationComponent
import com.example.activityinject.screens.base.BaseActivity

class App : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.activityComponent.inject(this)
    }

    fun tryInjectActivity(activity: BaseActivity<*, *>): Boolean {
        return applicationComponent.getActivityComponent(ActivityModule(activity))
            .activityInjector.maybeInject(activity)
    }

}
