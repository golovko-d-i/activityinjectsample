package com.example.activityinject.binding

typealias Listener = () -> Unit
