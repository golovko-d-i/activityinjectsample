package com.example.activityinject.di

import android.app.Activity
import com.example.activityinject.App
import dagger.Subcomponent
import dagger.android.AndroidInjectionModule
import dagger.android.DispatchingAndroidInjector

@Subcomponent(modules = [AndroidInjectionModule::class, ActivityModule::class])
interface ActivityComponent {

    val activityInjector: DispatchingAndroidInjector<Activity>

    fun inject(application: App)

}
