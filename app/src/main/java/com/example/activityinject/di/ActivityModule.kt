package com.example.activityinject.di

import android.app.Activity
import com.example.activityinject.screens.base.BaseActivity
import dagger.Module
import dagger.Provides
import dagger.android.support.AndroidSupportInjectionModule

@Module(includes = [(AndroidSupportInjectionModule::class)])
open class ActivityModule(private val activity: BaseActivity<*, *>) {

    @Provides
    fun provideActivity(): Activity = activity

}
