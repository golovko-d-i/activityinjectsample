package com.example.activityinject.di

import dagger.Component
import dagger.Subcomponent

@ApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    val activityComponent: ActivityComponent

    fun getActivityComponent(activityModule: ActivityModule): LocalActivityComponent

    @ActivityScope
    @Subcomponent(modules = [ActivityModule::class, ScreensModule::class])
    interface LocalActivityComponent : ActivityComponent

}
