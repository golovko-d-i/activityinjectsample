package com.example.activityinject.di

import com.example.activityinject.screens.fragment.SomeFragment
import com.example.activityinject.screens.fragment.SomeModule
import com.example.activityinject.screens.main.MainActivity
import com.example.activityinject.screens.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ScreensModule {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [SomeModule::class])
    abstract fun someFragment(): SomeFragment

}
