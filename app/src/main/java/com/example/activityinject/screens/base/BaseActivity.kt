package com.example.activityinject.screens.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.example.activityinject.App
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseActivity<VM : ViewModel, B : ViewDataBinding> : AppCompatActivity(), HasSupportFragmentInjector {

    private var fragmentInjector: DispatchingAndroidInjector<Fragment>? = null

    @Inject
    lateinit var vm: VM

    @get:LayoutRes
    protected abstract val layout: Int

    protected lateinit var dataBinding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as App).tryInjectActivity(this)
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(this), layout, null, false)
        dataBinding.lifecycleOwner = this
        setContentView(dataBinding.root)
        bindViewModel(dataBinding, vm)
    }

    protected abstract fun bindViewModel(binding: B, viewModel: VM)

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? = fragmentInjector

    @Inject
    fun injectDependencies(fragmentInjector: DispatchingAndroidInjector<Fragment>) {
        this.fragmentInjector = fragmentInjector
    }

}
