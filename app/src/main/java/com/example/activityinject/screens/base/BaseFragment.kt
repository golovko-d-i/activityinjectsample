package com.example.activityinject.screens.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseFragment<VM : ViewModel, B: ViewDataBinding> : Fragment(), HasSupportFragmentInjector {

    private var fragmentInjector: DispatchingAndroidInjector<Fragment>? = null
    @Inject
    lateinit var vm: VM

    @get:LayoutRes
    protected abstract val layout: Int

    protected lateinit var dataBinding: B

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? = fragmentInjector

    @Inject
    fun injectDependencies(fragmentInjector: DispatchingAndroidInjector<Fragment>) {
        this.fragmentInjector = fragmentInjector
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, layout, container, false)
        dataBinding.lifecycleOwner = this
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        AndroidSupportInjection.inject(this)
        bindViewModel(dataBinding, vm)
    }

    protected abstract fun bindViewModel(binding: B, viewModel: VM)

}
