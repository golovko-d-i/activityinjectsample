package com.example.activityinject.screens.fragment

import com.example.activityinject.R
import com.example.activityinject.databinding.FragmentSomeBinding
import com.example.activityinject.screens.base.BaseFragment

class SomeFragment : BaseFragment<SomeViewModel, FragmentSomeBinding>() {

    override val layout: Int = R.layout.fragment_some

    override fun bindViewModel(binding: FragmentSomeBinding, viewModel: SomeViewModel) {
        binding.viewModel = viewModel
    }

}

interface OnFragmentClickListener {

    fun onFragmentClicked()

}
