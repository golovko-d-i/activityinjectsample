package com.example.activityinject.screens.fragment

import android.app.Activity
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.activityinject.di.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class SomeModule {

    @Provides
    fun provideListener(activity: Activity): OnFragmentClickListener = object : OnFragmentClickListener {
        override fun onFragmentClicked() {
            Toast.makeText(activity, "Fragment listener provided via module", Toast.LENGTH_SHORT).show()
        }
    }

    @Provides
    @IntoMap
    @ViewModelKey(SomeViewModel::class)
    fun provideViewModel(vm: SomeViewModel): ViewModel = vm

}
