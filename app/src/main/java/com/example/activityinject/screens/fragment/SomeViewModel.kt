package com.example.activityinject.screens.fragment

import androidx.lifecycle.ViewModel
import com.example.activityinject.binding.Listener
import javax.inject.Inject

class SomeViewModel @Inject constructor(
    private val listener: OnFragmentClickListener
) : ViewModel() {

    val onClick: Listener = { listener.onFragmentClicked() }

}
