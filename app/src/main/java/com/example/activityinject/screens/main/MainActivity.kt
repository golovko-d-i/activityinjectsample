package com.example.activityinject.screens.main

import android.os.Bundle
import com.example.activityinject.R
import com.example.activityinject.databinding.ActivityMainBinding
import com.example.activityinject.screens.base.BaseActivity
import com.example.activityinject.screens.fragment.SomeFragment

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    override val layout: Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, SomeFragment())
            .commit()
    }

    override fun bindViewModel(binding: ActivityMainBinding, viewModel: MainViewModel) {
        binding.viewModel = viewModel
    }

}

interface OnSomethingClickListener {

    fun onSomethingClicked()

}
