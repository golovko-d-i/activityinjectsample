package com.example.activityinject.screens.main

import android.app.Activity
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.activityinject.di.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class MainModule {

    @Provides
    fun provideListener(activity: Activity): OnSomethingClickListener = object : OnSomethingClickListener {
        override fun onSomethingClicked() {
            Toast.makeText(activity, "Listener provided via module", Toast.LENGTH_SHORT).show()
        }
    }

    @Provides
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun provideViewModel(vm: MainViewModel): ViewModel = vm

}
