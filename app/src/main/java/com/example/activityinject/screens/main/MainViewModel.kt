package com.example.activityinject.screens.main

import androidx.lifecycle.ViewModel
import com.example.activityinject.binding.Listener
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val listener: OnSomethingClickListener
) : ViewModel() {

    val onClick: Listener = { listener.onSomethingClicked() }

}
